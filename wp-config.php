<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'wordpress_tousalacuisine' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', 'root' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'P>``$x}Ad$Z3{hTV18io4]-fn {FPD=w4A4<$S54D.Qv+iGK6LK;D;aF uM_Jy-F' );
define( 'SECURE_AUTH_KEY',  'hgqJNVt{!5hlwfx/VGXK;J8|!fiGEx:~9n]@QuWzz-cWLi%4{*5CT25t<s$EjwNE' );
define( 'LOGGED_IN_KEY',    'V1~Pe6tenTZ<]#CU]I(~53BSVg^3g-5(=WFCEor&y VYFMd9c=(h Y|TSw>g?qgq' );
define( 'NONCE_KEY',        'R~J ]!01z#t-zdWdjYuBSYtrlaMCq:MwL8OQJ_2lI2Qh<<(79zW8|&vNRANRZr+i' );
define( 'AUTH_SALT',        'yJRf#w_ppxj8CWT6W,hB~VUU|lpx{+A6/caFAF>*IZqzXKhXoi-QI;4]+h7dz#?(' );
define( 'SECURE_AUTH_SALT', 'l0&%,V2Gyn%Bu}JN1*X~,xaLX-<r~SHL`bjm)@1(L5*Xqld@ w g$ mXW~kCN(L-' );
define( 'LOGGED_IN_SALT',   'WH.PMnMRJ KiJa*Dl-,]!,lhMO E9!>l71ALs@QjOt}N1Ftdi cR~OUv(vroE(_+' );
define( 'NONCE_SALT',       'BD%vV92-c$D2{!2C.iT#BBrP3%cLbaIz=H)(I)|v},,bWO94-{{~XqV8?Si]63k*' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
