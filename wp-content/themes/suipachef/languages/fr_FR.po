msgid ""
msgstr ""
"Project-Id-Version: Suipachef\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-01-10 21:07+0000\n"
"PO-Revision-Date: 2017-01-10 21:07+0000\n"
"Last-Translator: superadmin <jcommaret@gmail.com>\n"
"Language-Team: \n"
"Language: fr_FR\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/"

#: archive.php:5
#, php-format
msgid "Daily Archives: %s"
msgstr ""

#: archive.php:6
#, php-format
msgid "Monthly Archives: %s"
msgstr ""

#: archive.php:7
#, php-format
msgid "Yearly Archives: %s"
msgstr ""

#: archive.php:8
msgid "Archives"
msgstr ""

#: attachment.php:6
#, php-format
msgid "Return to %s"
msgstr ""

#: author.php:5
msgid "Author Archives"
msgstr ""

#: category.php:4
msgid "Category Archives: "
msgstr ""

#: comments.php:31
msgid "Trackbacks"
msgstr ""

#: comments.php:31
msgid "Trackback"
msgstr ""

#: entry-footer.php:2
msgid "Categories: "
msgstr ""

#: entry-footer.php:5
msgid "Comments"
msgstr ""

#: functions.php:12
msgid "Main Menu"
msgstr ""

#: functions.php:42
msgid "Sidebar Widget Area"
msgstr ""

#. Name of the template
msgid "Index"
msgstr ""

#: nav-below.php:3
#, php-format
msgid "%s older"
msgstr ""

#: nav-below.php:4
#, php-format
msgid "newer %s"
msgstr ""

#: tag.php:4
msgid "Tag Archives: "
msgstr ""

#. Name of the theme
msgid "Suipachef"
msgstr ""

#. Author of the theme
msgid "Jérôme Commaret"
msgstr ""

#. Author URI of the theme
msgid "http://www.thecreative.cat/"
msgstr ""
