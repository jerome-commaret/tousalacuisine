<?php get_header(); ?>
  <?php if ( have_posts() ) : ?>
  <div class="col-md-12">
    <p><?php printf( __( 'Recettes contenant : %s', 'tousalacuisine' ), get_search_query() ); ?></p>
      <?php while ( have_posts() ) : the_post(); ?>
        <?php get_template_part( 'entry' ); ?>
            <?php endwhile; ?>
          <?php else : ?>
    </div>

    <div class="col-md-12">
      <p><?php _e( 'Nous n\'avons rien trouvé', 'tousalacuisine' ); ?></p>
      <p><?php _e( 'Retenter une recherche...', 'tousalacuisine' ); ?></p>
      <?php get_search_form(); ?>
    </div>

<?php endif; ?>
<?php get_footer(); ?>
