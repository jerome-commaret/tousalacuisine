<?php /* Template Name: Index */ ?>

<!--Insert header template-->
<?php get_header(); ?>

<!--Insert content template-->
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <p><?php the_field('quoteoftheday'); ?></p>
  <?php endwhile; endif; ?>

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="col-md-4 col-sm-6 middle">
      <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>">
        <img class="img-responsive" src="<?php the_field('miniature'); ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>"  />
      </a>
      <p class="postmetadata">Concocté le <?php the_time('j F Y') ?> par <?php the_author() ?></p>
    </div>
    <?php endwhile; endif; ?>
    <?php get_template_part( 'nav', 'below' ); ?>

<!--Insert footer template-->
<?php get_footer(); ?>
