<!--Start Include header-->
<?php get_header(); ?>
<!--End Include header-->

  <!--Start WP Loop-->
  <?php if(have_posts()) : ?>
      <?php while(have_posts()) : the_post(); ?>
      <div class="col-md-2"></div>
      <div class="col-md-8">
        <!--Title-->
        <h2><?php the_title(); ?></h2>
        <!--Instapic-->
        <div class="insta" style="float:none;"><?php the_field("lien_instagram"); ?></div>
      </div>
      <div class="col-md-2"></div>
      <!--Left panel prépa & ingredients-->
      <div class="col-md-4">
        <h3>Temps de préparation</h3>
        <!--Temps de préparation-->
        <p class="time"><?php the_field("temps_de_preparation"); ?></p>
        <h3 class="ingredientstitle">Les ingrédients</h3>
        <!--Ingrédients-->
        <div class="ingredients"><?php the_field("ingredients"); ?></div>
      </div>
      <!--Bloc recette-->
      <div class="col-md-8">
        <div class="recette">
          <h3>La recette</h3>
          <!--Recette-->
          <p><?php the_field("recette"); ?></p>
          <p>Cette recette est publiée en Creative Commons <a href="https://creativecommons.org/licenses/by-sa/2.0/fr/">CC-BY-SA</a></p>
        </div>
        <!--Biographie-->
        <div class="bioext">
          <?php if ( function_exists( 'get_author_bio_box' ) ) echo get_author_bio_box(); ?>
          <a href="https://www.instagram.com/suipachef/?ref=badge" class="instabadge"><img src="//badges.instagram.com/static/images/ig-badge-view-24.png" alt="Instagram" /></a>
        </div>
    </div>
      <?php endwhile; ?>
  <?php endif; ?>
  <!--End WP Loop-->

<!--Start Include footer-->
<?php get_footer(); ?>
<!--End Include header-->
