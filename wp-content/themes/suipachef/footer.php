</div>
<hr />
<div class="container">
  <p class="footer">© 2016 - 2017 | Suipachef | <a href="<?php bloginfo('url'); ?>/cookies">Cookies</a> | <a href="<?php bloginfo('url'); ?>/legal">Legal</a></p>
</div>
<!-- Google Analytics -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-89070417-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</body>
</html>
