<?php get_header(); ?>
  <h2 class="entry-title"><?php _e( 'Rien trouvé', 'tousalacuisine' ); ?></h1>
    <section class="entry-content">
      <p><?php _e( 'Nous n\'avons rien trouvé, tentez une recherche !', 'tousalacuisine' ); ?></p>
      <?php get_search_form(); ?>
    </section>
<?php get_footer(); ?>
