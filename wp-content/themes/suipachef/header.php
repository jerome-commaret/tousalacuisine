<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
  <!-- Basic Page Needs –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title><?php bloginfo('name');?> | <?php bloginfo('description');?></title>
  <meta name="description" content="<?php bloginfo('description');?>">
  <!-- Mobile Specific Metas –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
  <!-- FONT  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,600' rel='stylesheet' type='text/css'>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <!-- Custom CSS -->
  <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/custom.css">
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <!-- Favicon –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="<?php bloginfo('template_directory');?>/img/favicon.png">
</head>

<body class="code-snippets-visible" id="<?php the_id(); ?>">
  <!--Navbar-->
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <a class="navbar-brand" href="<?php bloginfo('url'); ?>">
        <img class="logo_small" src="<?php bloginfo('template_directory');?>/img/logo_rond_90.png"/>
        <img class="logo" src="<?php bloginfo('template_directory');?>/img/logo_rond_120.png"/>
      </a>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <form class="navbar-form navbar-right">
        <div class="form-group">
          <div class="right"><a href="#"><?php get_search_form(); ?></a></div>
      </form>
    </div>
  </nav>
  <!--Content-->
  <div class="container topfix">
