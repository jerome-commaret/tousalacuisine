<?php get_header(); ?>
<div class="docs-section" id="page">
  <div id="content">
    <?php if(have_posts()) : ?>
      <?php while(have_posts()) : the_post(); ?>
        <h2><?php the_title(); ?></h2>
        <?php the_content(); ?>
      </div>
    </div>
    <?php endwhile; ?>
    <?php endif; ?>
<?php get_footer(); ?>
